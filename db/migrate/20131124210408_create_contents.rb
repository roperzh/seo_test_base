class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.text      :instagram_id
      t.text      :caption
      t.text      :user_name
      t.text      :user_icon
      t.text      :img_standard_resolution
      t.text      :likes
      t.text      :comments

      t.timestamps
    end
  end
end
