Base::Application.routes.draw do

  resources :server_side, only: [:index, :show]
  resources :hybrid,      only: [:index, :show]
  resources :client_side, only: [:index, :show]

  root to: "home#index"
end
