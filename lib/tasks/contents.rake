namespace(:content) do
  desc "Fech recent content from Instagram"
  task :fetch => :environment do
    puts ">> Fetching data..."

    ContentFetcher.by_tags(['javascript'])

    puts "\n>> Done!"
  end
end
