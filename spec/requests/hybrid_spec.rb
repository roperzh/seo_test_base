require 'spec_helper'

feature 'Hybrid strategy workflow' do
  before :each do
    create(:content)
    visit hybrid_index_path
  end

  scenario 'show all items' do
    expect(page).to have_content Content.first.likes
  end

  scenario 'show specific item and go back to all items view', js: true do
    first(:link, 'View more').click
    expect(current_path).to eq hybrid_path(1)
    click_link '<- Go Back'
    expect(current_path).to eq hybrid_index_path
  end
end
