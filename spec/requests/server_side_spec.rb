require 'spec_helper'

feature 'Server side workflow' do
  before :each do
    20.times do
      create(:content)
    end
  visit server_side_index_path
  end

  scenario 'show all items in server side section' do
    expect(page).to have_content Content.first.likes
  end

  scenario 'show specific item' do
    first(:link, 'View more').click
    expect(current_path).to eq server_side_path(1)
  end
end
