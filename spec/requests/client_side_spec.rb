require 'spec_helper'

feature 'Client side workflow' do
  before :each do
    create(:content)
    visit client_side_index_path
  end

  scenario 'show all items in server side section', js: true do
    expect(page).to have_content Content.first.likes
  end

  scenario 'show specific item', js: true do
    first(:link, 'View more').click
    expect(current_path).to eq client_side_path(1)
    click_link '<- Go Back'
    expect(current_path).to eq client_side_index_path
  end
end
