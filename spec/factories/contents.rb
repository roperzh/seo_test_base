FactoryGirl.define do
  factory :content do
    instagram_id              5488787
    caption                   'awsome caption #'
    user_name                 'Rabdul'
    user_icon                 '/50x50.gif'
    img_standard_resolution   '/50x50.gif'
    likes                     22
    comments                  23
  end
end
