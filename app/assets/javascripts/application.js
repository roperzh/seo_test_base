//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require underscore
//= require json2
//= require backbone
//= require backbone.marionette
//= require backbone.routefilter
//= require initializers
//= require app
//= require apps/photo_wall/photo_wall_app
//= require apps/photo_wall/photo_wall_controllers
//= require apps/photo_wall/photo_wall_views
//= require mustache
//= require_tree ../../templates

PhotoWall.start();
