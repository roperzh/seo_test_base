var PhotoWall = new Marionette.Application();

PhotoWall.addRegions({
  mainRegion: '#page-wrapper'
});

//Navigate to specific route
PhotoWall.navigate = function(route, options) {
  options || (options = {});
  Backbone.history.navigate(route, options);
};

//Returns current route
PhotoWall.getCurrentRoute = function() {
  return Backbone.history.fragment;
};

//Things to do after app initialize
PhotoWall.on('initialize:after', function() {
  if (Backbone.history) {
    Backbone.history.start({
      pushState: true
    });
  }
});
