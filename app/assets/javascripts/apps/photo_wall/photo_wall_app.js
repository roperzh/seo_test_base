PhotoWall.module('Wall', function(Wall, PhotoWall, Backbone, Marionette, $, _) {

  Wall.Router = Backbone.Marionette.AppRouter.extend({
    appRoutes: {
      'client_side(/)': 'showCollection',
      'client_side/:id(/)': 'showItem',
      'hybrid(/)': 'showCollection',
      'hybrid/:id(/)': 'showItem'
    },

    before: {
      "hybrid": function(route, params) {
        if (firstLoad === true) {
          var view = new Wall.popupsView({
            el: $("#statics-attachment")
          });

          PhotoWall.mainRegion.attachView(view);
          firstLoad = false;
          return false;
        }
      },
      "hybrid/:id": function(route, params) {
        if (firstLoad === true) {
          firstLoad = false;
          return false;
        }
      }
    }
  });

  var API = {
    showCollection: function() {
      console.log('renders collection');
      PhotoWall.execute('collection:index');
    },

    showItem: function(id) {
      console.log('renders item');
      PhotoWall.execute('collection:show', parseInt(id, 10));
    }
  };

  Wall.addInitializer(function() {
    router = new Wall.Router({
      controller: API
    });
  });
});
