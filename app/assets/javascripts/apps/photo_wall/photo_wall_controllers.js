PhotoWall.module('Wall', function(Wall, PhotoWall, Backbone, Marionette, $, _) {
  Wall.Controller = {
    showCollection: function() {
      model = new Backbone.Model(window.gon);
      view = new Wall.popupsView({
        model: model
      });
      PhotoWall.mainRegion.show(view);
    },

    showItem: function(id) {
      var item = new Backbone.Model(_.where(gon.contents, {
        id: id
      })[0]),
        itemView = new Wall.itemView({
          model: item
        });
      PhotoWall.mainRegion.show(itemView);
    }
  };

  PhotoWall.commands.setHandler('collection:index', function() {
    Wall.Controller.showCollection();
  });

  PhotoWall.commands.setHandler('collection:show', function(id) {
    Wall.Controller.showItem(id);
  });
});
