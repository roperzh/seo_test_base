PhotoWall.module('Wall', function(Wall, PhotoWall, Backbone, Marionette, $, _) {
  Wall.popupsView = Marionette.ItemView.extend({
    template: 'shared/contents',
    events: {
      'click .js-viewMore': 'showItem'
    },

    showItem: function(e) {
      e.preventDefault();
      var id = $(e.currentTarget).data('id');
      PhotoWall.execute('collection:show', id);
      PhotoWall.navigate('/' + currentState + '/' + id);
    }
  });

  Wall.itemView = Marionette.ItemView.extend({
    template: 'shared/item',
    events: {
      'click .js-backButton': 'showCollection'
    },
    showCollection: function(e) {
      PhotoWall.execute('collection:index');
      PhotoWall.navigate('/' + currentState);
    },
  });
});
