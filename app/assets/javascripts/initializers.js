Backbone.Marionette.Renderer.render = function(template, data) {
	if (!SMT[template]) throw "Template '" + template + "' not found!";
	return SMT[template](data);
};
