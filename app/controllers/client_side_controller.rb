class ClientSideController < ApplicationController
  expose(:contents)

  def index
    @contents = Content.all
    gon.rabl template: 'app/views/contents/contents.rabl'
  end
end
