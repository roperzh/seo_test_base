class HybridController < ApplicationController
  expose(:contents)
  expose(:content)

  def index
    @contents = Content.all
    gon.rabl template: 'app/views/contents/contents.rabl'
  end

  def show; end
end
