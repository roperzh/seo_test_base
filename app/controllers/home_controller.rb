class HomeController < ApplicationController

  expose(:contents)

  def index; end
end
