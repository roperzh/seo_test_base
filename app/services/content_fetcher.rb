class ContentFetcher
  def self.by_tags(tags)
    for tag in tags do
      for content in Instagram.tag_recent_media(tag) do
        create_content(content)
        print "."
      end
    end
  end

  def self.create_content(content)
    Content.where(instagram_id: content.id).first_or_create! do |c|
      c.instagram_id              = content.id
      c.caption                   = content.caption.text if content.caption
      c.user_name                 = content.user.username if content.user
      c.user_icon                 = content.user.profile_picture
      c.img_standard_resolution   = content.images.standard_resolution.url
      c.likes                     = content.likes[:count]
      c.comments                  = content.comments[:count]
    end
  end
end
